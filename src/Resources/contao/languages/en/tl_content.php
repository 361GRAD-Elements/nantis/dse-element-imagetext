<?php

/**
 * 361GRAD Element Image-Text
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_imagetext'] = ['Image/Text', 'Image left|right and Text right|left'];

$GLOBALS['TL_LANG']['tl_content']['headline_legend']   = 'Headline settings';
    
$GLOBALS['TL_LANG']['tl_content']['dse_linkTitle']   = ['Link title', 'Tooltip title will be visible on hover'];
$GLOBALS['TL_LANG']['tl_content']['dse_titleText']   = ['Link text', 'Text of the link'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];