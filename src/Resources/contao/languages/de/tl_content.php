<?php

/**
 * 361GRAD Element Image-Text
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_imagetext'] = ['Bild/Text', 'Bild links|rechts und Text rechts|links'];

$GLOBALS['TL_LANG']['tl_content']['headline_legend']   = 'Schlagzeileneinstellungen';

$GLOBALS['TL_LANG']['tl_content']['dse_linkTitle']   = ['Titel des Links', 'Der Tooltip-Titel wird auf dem Mauszeiger angezeigt.'];
$GLOBALS['TL_LANG']['tl_content']['dse_titleText']   = ['Link text', 'Text des Links'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];