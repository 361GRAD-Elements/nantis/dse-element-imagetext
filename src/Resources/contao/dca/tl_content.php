<?php

/**
 * 361GRAD Element Image-Text
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_imagetext'] =
    '{type_legend},type;' .
    '{headline_legend},headline;' .
    '{text_legend},text;' .
    '{image_legend},addImage;' .
    '{link_legend},url,target,dse_linkTitle,dse_titleText;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop';

// Element subpalettes
$GLOBALS['TL_DCA']['tl_content']['subpalettes']['addImage'] = 'singleSRC,alt,title,size,imagemargin,fullsize,floating';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['floating']['options']      = ['left', 'right'];
$GLOBALS['TL_DCA']['tl_content']['fields']['url']['eval']['mandatory'] = false;

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_linkTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_linkTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_titleText'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_titleText'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop']    = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
